import {Injectable} from '@angular/core';

@Injectable()
export class AuthService {
    token: string;
    redirectUrl: string;

    isAuthenticated() {
        return true; // this.token != null;
    }
}
