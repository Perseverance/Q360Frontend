import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdvancedVesselSearchComponent} from './advanced-vessel-search.component';


@NgModule({
    declarations: [
        AdvancedVesselSearchComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        AdvancedVesselSearchComponent
    ]
})
export class AdvancedVesselSearchModule {
}
