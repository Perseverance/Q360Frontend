import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MyFleetSearchComponent} from './my-fleet-search.component';


@NgModule({
    declarations: [
        MyFleetSearchComponent
    ],
    imports: [
        CommonModule
    ],
    exports: [
        MyFleetSearchComponent
    ]
})
export class MyFleetSearchModule {
}
