import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyFleetSearchComponent } from './my-fleet-search.component';

describe('MyFleetSearchComponent', () => {
  let component: MyFleetSearchComponent;
  let fixture: ComponentFixture<MyFleetSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyFleetSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyFleetSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
