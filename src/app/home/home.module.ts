import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {RouterModule} from '@angular/router';
import {HomeRoutingModule} from './home-routing.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {SharedModule} from '../shared/shared.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        HomeRoutingModule,
        NgxDatatableModule,
        SharedModule
    ],
    declarations: [
        HomeComponent]
})
export class HomeModule {
}
