import {Component, ViewEncapsulation} from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class HomeComponent {

    // testing purpose
    rows = [
        {id: 1, name: 'Draft 1', vesselName: 'Vesel 123135354654634432657665885564563543525342'},
        {name: 'Draft 2', vesselName: 'Vesel 2'},
        {name: 'Draft 3', vesselName: 'Vesel 3'},
        {name: 'Draft 4', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 1', vesselName: 'Vesel 1'},
        {name: 'Draft 2', vesselName: 'Vesel 2'},
        {name: 'Draft 3', vesselName: 'Vesel 3'},
        {name: 'Draft 4', vesselName: 'Vesel 4'},
        {id: 1, name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'},
        {name: 'Draft 3', vesselName: 'Vesel 4'}
    ];
    columns = [
        {
            prop: 'name',
            name: 'My drafts'
        },
        {
            prop: 'vesselName',
            name: 'Vessel Name'
        },
    ];
}
