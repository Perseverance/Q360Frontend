import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AdvancedVesselSearchComponent} from '../advanced-vessel-search/advanced-vessel-search.component';
import {MyFleetSearchComponent} from '../my-fleet-search/my-fleet-search.component';
import {HomeComponent} from './home.component';
import {AuthGuard} from '../auth/auth-guard.service';


const homeSearchRoutes: Routes = [
        {path: '', pathMatch: 'full', component: HomeComponent, canActivate: [AuthGuard]},
        {path: 'advanced-vessel-search', component: AdvancedVesselSearchComponent, canActivate: [AuthGuard]},
        {path: 'my-fleet-search', component: MyFleetSearchComponent, canActivate: [AuthGuard]}
    ]
;

@NgModule({
    imports: [
        RouterModule.forChild(homeSearchRoutes)
    ],
    exports: [RouterModule]
})
export class HomeRoutingModule {
}
