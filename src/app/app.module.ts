import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {CoreModule} from './core/core.module';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {AuthModule} from './auth/auth.module';
import {SigninModule} from './signin/signin.module';
import {HomeModule} from './home/home.module';
import {AdvancedVesselSearchModule} from './advanced-vessel-search/advanced-vessel-search.module';
import {MyFleetSearchModule} from './my-fleet-search/my-fleet-search.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        CoreModule,
        AuthModule,
        AdvancedVesselSearchModule,
        MyFleetSearchModule,
        HomeModule,
        AppRoutingModule,
        SigninModule
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
